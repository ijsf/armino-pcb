(DEVICE FILE: SM0402L)

PACKAGE SM0402L
CLASS DISCRETE
PINCOUNT 2

PINORDER SM0402L 'TP-1' 'TP-2'
FUNCTION 'TS-1' SM0402L 1 2

PACKAGEPROP VALUE '120-L2-X'

END
