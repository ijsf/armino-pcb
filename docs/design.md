Bill-of-materials for enclosure, stage 1:

* 1x **Schroff 19" (84HP) x 3U x 355mm europac Pro flexible connector mounting unshielded** - 24563-194 or 134 (Schroff), 39.50 EUR/pc (RS) or 54.00 (Distrelec)
* 2x (20) **Schroff 19" card guide 280mm** - 24560-379 (Schroff), 17.50 EUR/10pcs (Distrelec)

Bill-of-materials for enclosure, stage 2:

* 2x **Schroff Cover panel for top/bottom unshielded** - 24560-081 (Schroff), 45.20 EUR/pc (Distrelec)
* 4x **60x60mm 12VDC Fan** - 2509697470 (RS), 5.12 EUR/pc (RS) 
* 1x **Schroff Front panel unshielded** (for rear) - 30818-352 (Schroff), 14.10 EUR/pc (Distrelec)
* 2x **Schroff Front horizontal rail** (for rear) - 34560-184 (Schroff), 5.18 EUR/pc (Distrelec)
* 1x (10) **60x60mm fan guard** - 737-3973 (RS), 9.16 EUR/10pcs (RS) 
* *TODO: 19" mounting accessories*

Units:

* 1U (or HE, rack Unit) = 1.75" = 44.45mm
* 1HP (Horizontal Pitch) = 0.2" = 5.08mm

Clarification of coordinates:

* Height: top to bottom (e.g. 3U) when looking from the front of the rack unit.
* Width: distance between one module and the next adject module, left to right (e.g. 5HP).
* Length: depth of the board from front to back (e.g. 280mm).

Bill-of-materials for control module:

* *TODO: switch*
* *TODO: hot swap controller*
* *TODO: microcontroller*

Bill-of-materials for board module:

* 1x **EDMCONNECTORKIT** (Official kit, per 10pcs) or **MM70-314-310B1** - 670-2431-1-ND (Digikey), 6.88 EUR/pc (Digikey)
* 1x **WB-EDM-IMX6Q-10PACK (Wandboard Quad EDM module)** - dimensions ~85mm x 63mm
* 1x **XLR panel mount connector** - 457-936 (RS), 1.41 EUR/pc (RS) - dimensions 24mm length (from panel), 23.4mm hole radius, ~31mm width
* 1x **TE Connectivity 1761465 R/A PCIe x1 connector** - A108522-ND (Digikey), 6.17 EUR/pc (Digikey)
* 1x **Molex 8981 female PCB header** - 670-4250 (RS) - 3.61 EUR/10pcs (RS)
* 1x **TE 171825-4 4 pin PCB header** - 718-8532 (RS) - 1.62 EUR/5pc (RS)
* 1x **Molex 67800-5011 Serial ATA vertical header**
* 1x **Low-profile Intel/Broadcom Gigabit Ethernet PCIe x1 controller**
* 1x **FT232RL FTDI USB UART IC** - 768-1007-1-ND (Digikey) - 3.60 EUR/pc (Digikey)
* 1x **TE 1981584-1 USB Micro AB receptacle** - A97799CT-ND (Digikey) - 1.44 EUR/pc (Digikey)
* 1x **Panasonic 4TPF330ML Tantalum capacitor 330uF 4V** - P16222CT-ND (Digikey) - 2.01 EUR/pc (Digikey)
* 1x **Panasonic 6TPF330M9L Tantalum capacitor 330uF 6.3V** - P16258CT-ND (Digikey) - 2.11 EUR/pc (Digikey)
* 2x **International Rectifier IRF7811AVTRPBF MOSFET 30V 10.8A SOIC8** - IRF7811AVPBFCT-ND (Digikey) - 1.07 EUR/pc (Digikey)
* 2x **International Rectifier IRF7807ZTRPBF MOSFET 30V 11A SOIC8** - IRF7807ZPBFCT-ND (Digikey) - 0.93 EUR/pc (Digikey)
* 1x **Nano 452 Littelfuse 5.0A** - F1862CT-ND (Digikey) - 1.25 EUR/pc (Digikey)
* 1x **Panasonic ELL-6SH4R7M 4.7uH 2.0A inductor** - PCD1366CT-ND (Digikey) - 0.90 EUR/pc (Digikey)
* 1x **Panasonic ELL-6SH6R8M 6.8uH 1.5A inductor** - PCD1368CT-ND (Digikey) - 0.90 EUR/pc (Digikey)
* 1x **Molex 0678008025 S-ATA 7 pos vertical connector** - WM7699-ND (Digikey) - 0.68 EUR/pc (Digikey)
* TODO: LED on cable with female header.
* Various ceramic capacitors in SMD 1206 and 0805 form factors.
* Various resistors in SMD 1206 and 0805 form factors.
* Various ferrite beads in SMD 0805 form factors.

Bill-of-materials for PSU module:

* 2x **12VDC PSU 200W** <100mm in height - any Chinese supplier through e.g. eBay, ~16 EUR/pc
* 1x **IEC connector** - any supplier

Manufacturing requirements for control module (>=?mm length):

* Height must not exceed 3U (~125mm)

### Board module ###

#### Revisions and changes ####

##### Rev 1.1 ####
Improvements:

* Exposed copper for both power pads POWER1 and POWER2.
* Less full contact thermals (screwing up the pin spacing).

##### Rev 1.0 ####
PCB produced.

#### Module layout ####
Width is 7HP (35.56mm), from left to right:

1. (1.00mm): 3D printed carrier tray
2. (15.24mm): PCIe Ethernet controller (low profile form factor)
3. (18.50mm): Carrier board + EDM module + cooling margin

Length is 280mm, matching provided 280mm guide rails, from front to back:

1. (130.00mm): Carrier board + EDM module + PCIe Ethernet controller (low profile form factor must not exceed dimensions!)
2. (150.00mm): Hard drive (must not exceed dimensions!)

Height is 3U (125.00mm), from top to bottom:

1. (79.2mm): PCIe Ethernet controller (low profile form factor total bracket height, connector opening 42.47+12.07=54.54mm, board slot end to bracket far underside 79.20-(9.04+54.53)=15.63mm)
2. (25.0mm): XLR panel mount connector + margin
3. (20.8mm): Spacing?

#### Front panel layout ####

* XLR panel mount connector must be mounted sideways, so that the width requirement for this connector is only 25.5mm. Depth after panel mount part is 24mm and height is 23mm, this should be cut out on PCB for more margin.
* Must provide facilities to mount and fasten low profile form factor PCIe controller.
* USB Micro B host port for USB->UART (FT232) serial connection.

#### Carrier board features ####

* XLR power connector for hot plugging.
* Soft starting (hot pluggable) switching step down converter 12V -> 5V and 3.3V supplying S-ATA (5V), PCIe (3.3V) and EDM module (5V).
* S-ATA data connector.
* S-ATA power connector through TE 171825-4.
* PCIe x1 connector.
* FT232 USB->UART serial conversion for debugging purposes.
* Header for power LED.

#### Carrier board layout ####

* Chamfered PCB edges.
* Mounting holes in PCB.
* PCIe to board edge must match standard PCIe to slot specification dimensions.
* Mounting nuts for EDM card required, placement data from Technexion/Wandboard reference designs: distances from hole in connector to respectively first and second mounting nuts are (11.50mm, 3.52mm) and (56.5mm, 3.52mm).
* PCIe mid hole to end of card distance is 56.21mm, mid hole to bracket mounting point distance is 64.44mm. End of card to bracket distance is 1.02mm. Bracket mounting ear depth is 11.84mm. PCIe mid hole must thus be located at least (56.21+1.02+11.84<=) 70mm from end of PCB to provide for proper PCIe bracket mounting.

#### Carrier board electrical considerations ###

* Low cost 2-layer PCB possible.
* Careful layout required with differential pairs for PCIe and S-ATA. Shortest traces possible.
* Extensive GND planes on both sides.

### PSU module ###

Manufacturing requirements for PSU module (>=200mm length):

* Width typically within 10 HP (<50.8mm)
* Height must not exceed 3U (~125mm)
* Dimensions of typical 12VDC 200W PSU: 200x98x50mm
* IEC connector (~23mm height)

Manufacturing requirements for enclosure:

* Rear panel is solid and must provide accomodation for 4x 60x60mm fans
* Board module mounting plate for module (ARM board, baseboard, harddrive), 3d printed
* PSU module mounting plate, 3d printed

